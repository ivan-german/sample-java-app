public class App {
    public static void main(String[] args) {
        int a = 8;
        {
            int b = 3;
            System.out.println(a);  // Выведет 15
            System.out.println(b);  // Выведет 3
        }
        int a = 16;  // ОШИБКА: в области видимости уже есть переменная с именем a.
        System.out.println(a); // выведет 8
        System.out.println(b); // ОШИБКА: в области видимости нет переменной b
    }
}
